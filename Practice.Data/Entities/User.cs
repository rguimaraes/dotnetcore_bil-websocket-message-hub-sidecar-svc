﻿namespace Data.Entities
{
    public class User 
    {
        public User(int idUser, int profile_Id, int operatorGroup_Id, int operator_Id, int customer_Id, int hall_Id, string language)
        {
            IdUser = idUser;
            Profile_Id = profile_Id;
            OperatorGroup_Id = operatorGroup_Id;
            Operator_Id = operator_Id;
            Customer_Id = customer_Id;
            Hall_Id = hall_Id;
            LanguageId = (int)GetLanguageID(language);
        }


        public int IdUser { get; set; }
        public int Profile_Id { get; set; }
        public int OperatorGroup_Id { get; set; }
        public int Operator_Id { get; set; }
        public int Customer_Id { get; set; }
        public int Hall_Id { get; set; }
        public int LanguageId { get; set; }


        public enum eUser
        {
            Invalid = 0,
            Master = 1,
            Operator_Group = 2,
            Operator = 3,
            Customer = 4,
            Hall = 5
        }

        private EnLanguage GetLanguageID(string language)
        {
            EnLanguage idLanguage;
            switch (language)
            {
                case "pt-BR":
                    idLanguage = EnLanguage.Portuguese;
                    break;
                case "es-ES":
                    idLanguage = EnLanguage.Spanish;
                    break;
                default:
                    idLanguage = EnLanguage.English;
                    break;
            }
            return idLanguage;
        }

        public enum EnLanguage
        {
            Portuguese = 1,
            Spanish = 2,
            English = 3

        }



        public eUser GetTypeAccess()
        {
            if (this.Hall_Id == int.MinValue && this.Customer_Id == int.MinValue && this.Operator_Id == int.MinValue
               && this.OperatorGroup_Id == int.MinValue)
                return eUser.Master;
            else if (this.Hall_Id == int.MinValue && this.Customer_Id == int.MinValue && this.Operator_Id == int.MinValue
                && this.OperatorGroup_Id > 0)
                return eUser.Operator_Group;
            else if (this.Hall_Id == int.MinValue && this.Customer_Id == int.MinValue && this.Operator_Id > 0
                    && this.OperatorGroup_Id == int.MinValue)
                return eUser.Operator;
            else if (this.Hall_Id == int.MinValue && this.Customer_Id > 0 && this.Operator_Id > 0
                    && this.OperatorGroup_Id == int.MinValue)
                return eUser.Customer;
            else if (this.Hall_Id > 0 && this.Customer_Id > 0 && this.Operator_Id > 0
                    && this.OperatorGroup_Id == int.MinValue)
                return eUser.Hall;


            return eUser.Invalid;

        }




    }
}
