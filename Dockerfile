FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["Message.Hub.Api/Message.Hub.Api.csproj", "SystemNet.Portal.Api/"]
COPY ["SystemNet.Core.Infraestructure/SystemNet.Core.Infraestructure.csproj", "SystemNet.Core.Infraestructure/"]
COPY ["SystemNet.Core.Domain/SystemNet.Core.Domain.csproj", "SystemNet.Core.Domain/"]
COPY ["SystemNet.Practices.Data/SystemNet.Practices.Data.csproj", "SystemNet.Practices.Data/"]
COPY ["SystemNet.Practice.Common/SystemNet.Practice.Common.csproj", "SystemNet.Practice.Common/"]
COPY ["SystemNet.Shared/SystemNet.Shared.csproj", "SystemNet.Shared/"]
COPY ["SystemNet.Business/SystemNet.Business.csproj", "SystemNet.Business/"]
RUN dotnet restore "Message.Hub.Api/Message.Hub.Api.csproj"
COPY . .
WORKDIR "/src/Message.Hub.Api"
RUN dotnet build "Message.Hub.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Message.Hub.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Message.Hub.Api.dll"]