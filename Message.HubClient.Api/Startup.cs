﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Data.Entities;
using System.IO.Compression;
using Common.AutoConfigurable.Logging;
using Message.Hub.Api.HubEndPoints;
using Message.Hub.Api.HubEndPoints.Base;
using Message.Hub.Api.HubServiceExtensions;
using Domain.Options;
using SystemNet.Practice.Token.Util;
using Common.AutoConfigurable.Api.Swagger.Configurations;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace Message.Hub.Api
{
    /// <summary>
    /// Configuration of API
    /// </summary>
    public class Startup
    {
        public static string SecreteKey = "18008307-ad68-4954-add0-f292c481a529";
        public IConfiguration Configuration { get; set; }

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = configurationBuilder.Build();
        }

        /// <summary>
        /// Set the configuration for the API
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            TokenConfigureServices.ConfigureToken(ref services, SecreteKey);

            System.Diagnostics.Debugger.Launch();

            services.AddMvc();
            services.AddSwaggerDoc();
            services.AddOptions().Configure<HubClientOptions>(Configuration.GetSection(HubClientOptions.OPTIONS_PREFIX));

            // Add compression
            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Optimal;
            });


            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });

            // Active the HeathChecks
            services.AddHealthChecks();
            services.AddResponseCompression();
            services.AddResponseCaching();
            services.AddCors();
            services.AddSingleton(Configuration);
            services.AddTransient<User, User>();
            services.AddAutoConfigurableLogging();


            services.AddHubExtension();
            services.AddSingleton<BaseHubEndPoint>();
            services.AddSingleton<ClientHubEndPoint>();
            services.BuildServiceProvider(true).GetService<BaseHubEndPoint>();
            services.BuildServiceProvider(true).GetService<ClientHubEndPoint>();

        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            app.UseSwaggerDoc(provider);

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.SetIsOriginAllowed(_ => true);
                x.AllowCredentials();
            });

            app.UseResponseCaching();
            app.UseHealthChecks("/status");
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();

        }
    }


}
