﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using SystemNet.Practice.Token.Filters;
using Microsoft.AspNetCore.Authorization;

namespace Message.Hub.Api.Controllers
{
    /// <summary>
    /// Controller to management connected clients 
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]
    public class ClientsController : BaseController
    {
        ILogger<ClientsController> _ilog;

        public ClientsController( ILogger<ClientsController> ilog) : base(ilog)
        {
            _ilog = ilog;
        }

        /// <summary>
        /// Get all currently connected clients on Hub
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v{version:apiVersion}/clients")]
        [Authorize(Policy = "Member")]
        [MiddlewareFilter(typeof(CompressionGZipPipeline))]
        public IActionResult GetAll()
        {
            try
            {
                return Ok();
            }
            catch (Exception ex)
            {
                _ilog.LogError(ex, "Error ClientsController");
                return BadRequest(new
                {
                    success = false,
                    errors = new[] { ex }
                });
            }
        }


    }
}
