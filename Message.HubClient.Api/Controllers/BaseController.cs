﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Data.Entities;
using Message.Hub.Api.Enums;
using System.Security.Claims;
using Common.Base;
using static Common.Base.LanguageFunctions;
using Microsoft.Extensions.Logging;

namespace Message.Hub.Api.Controllers
{
    /// <summary>
    /// Class Base to back the response to the front ed
    /// </summary>
    public abstract class BaseController : ControllerBase
    {
        ILogger<BaseController> _ilog;

        public BaseController(ILogger<BaseController> ilog)
        {
            _ilog = ilog;
        }

        /// <summary>
        /// Return Custom Forbidden
        /// </summary>
        /// <returns></returns>
        protected  ObjectResult Forbidden(object result)
        {
            return  StatusCode((int)System.Net.HttpStatusCode.Forbidden, new
            {
                success = false,
                data = result
            });
        }

        /// <summary>
        /// Return the result of API
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        protected new IActionResult Response(object result)
        {
            try
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }
            catch (Exception ex)
            {
                _ilog.LogError(ex, "Error BaseController");
                return BadRequest(new
                {
                    success = false,
                    errors = new[] { "Ocorreu uma falha interna no servidor." }
                });
            }
        }



        /// <summary>
        /// Get an user data from the Token 
        /// </summary>
        /// <returns></returns>
        protected User GetUser()
        {
            return new User(GetDataAccessToken(Claims.UserId), GetDataAccessToken(Claims.Profile), GetDataAccessToken(Claims.OperatorGroup_Id),
                GetDataAccessToken(Claims.Operator_Id), GetDataAccessToken(Claims.Customer_Id), GetDataAccessToken(Claims.Room_Id),
                System.Globalization.CultureInfo.CurrentCulture.Name);
        }

        /// <summary>
        /// Get user code from the token
        /// </summary>
        /// <returns></returns>
        protected int GetUserToken()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = HttpContext.User.Claims;
            return Convert.ToInt32(identity.Claims.ElementAt((int)Claims.UserId).Value);
        }

        /// <summary>
        /// Get login name from the token
        /// </summary>
        /// <returns></returns>
        protected string GetLoginToken()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = HttpContext.User.Claims;
            return identity.Claims.ElementAt((int)Claims.Login).Value;
        }

        /// <summary>
        /// Get specific parameter from the Token
        /// </summary>
        /// <param name="typeClaim"></param>
        /// <returns></returns>
        protected int GetDataAccessToken(Claims typeClaim)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = HttpContext.User.Claims;
            return Convert.ToInt32(identity.Claims.ElementAt((int)typeClaim).Value);
        }

        /// <summary>
        /// Get the language sent in api request
        /// </summary>
        /// <returns></returns>
        protected Language GetRequestLanguage()
        {
            return LanguageFunctions.GetLanguageID(System.Globalization.CultureInfo.CurrentCulture.Name);
        }
    }
}
