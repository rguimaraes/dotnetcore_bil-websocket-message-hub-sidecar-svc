﻿namespace Message.Hub.Api.Enums
{
    public enum Claims
    {
        UserId = 2,
        UserName = 0,
        GivenNameUser = 1,
        Login = 3,
        Profile = 4,
        OperatorGroup_Id = 5,
        Operator_Id = 6,
        Customer_Id = 7,
        Room_Id = 8
    }
}
