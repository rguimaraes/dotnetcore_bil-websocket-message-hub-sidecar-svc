﻿using Domain.Options;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using System;

namespace Message.Hub.Api.HubServiceExtensions
{
    public static class HubServiceExtensions
    {
        public static IServiceCollection AddHubExtension(this IServiceCollection services)
        {
            using (var serviceProvider = services.BuildServiceProvider())
            {
                HubClientOptions options = serviceProvider.GetService<IOptions<HubClientOptions>>().Value;

                HubConnectionBuilder _connection = new HubConnectionBuilder();

                _connection.WithUrl(options.UrlServer)
                     .WithAutomaticReconnect(new RetryConnect(options.RetryConnectDelay))
                     .Build();

                services.Add(_connection.Services);

                return services;
            }
        }
    }

    public class RetryConnect : IRetryPolicy
    {
        double _retryConnectDelay;

        public RetryConnect(double retryConnectDelay)
        {
            _retryConnectDelay = retryConnectDelay;
        }

        public TimeSpan? NextRetryDelay(RetryContext retryContext)
        {
            return TimeSpan.FromSeconds(_retryConnectDelay);
        }
    }
}
