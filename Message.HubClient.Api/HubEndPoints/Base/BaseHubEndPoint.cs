﻿using System;
using System.Threading.Tasks;
using Domain.Options;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Message.Hub.Api.HubEndPoints.Base
{
    public class BaseHubEndPoint
    {
        private ILogger<BaseHubEndPoint> _ilog;
        
        private HubClientOptions _hubClientOptions;
        protected static HubConnection Hubconnection { get; private set; }
        public BaseHubEndPoint() { }

        public BaseHubEndPoint(HubConnection hubConnection, ILogger<BaseHubEndPoint> ilog, IOptions<HubClientOptions> hubClientOptions)
        {
            _hubClientOptions = hubClientOptions.Value;
            Hubconnection = hubConnection;
            Hubconnection.ServerTimeout = TimeSpan.FromDays(_hubClientOptions.ServerTimeout);
            _ilog = ilog;
            SignEvents();
        }

        public async void SignEvents()
        {
            try
            {  
                while (Hubconnection.State != HubConnectionState.Connected)
                {
                    try
                    {
                        await Hubconnection.StartAsync();
                    }
                    catch (Exception ex)
                    {
                        _ilog.LogError(ex, "Error Connection Hub");
                    }

                    System.Threading.Thread.Sleep((int)_hubClientOptions.RetryConnectDelay *1000);
                }

                _ilog.LogInformation($"Connection Successful clientid"+ Hubconnection.ConnectionId);

                Hubconnection.Closed += (exception) =>
                {
                    if (exception == null)
                        _ilog.LogInformation("Connection closed without error.");
                    else
                        _ilog.LogError(exception, $"Connection closed due to an error");
                    
                    return Task.CompletedTask;
                };

                Hubconnection.Reconnected += (clientid) => {

                    _ilog.LogInformation($"Reconnected Successful clientid"+ clientid);
                    return Task.CompletedTask;
                };

            }
            catch(Exception ex)
            {
                _ilog.LogError(ex, "General Error");
            }

        }
    }
}
