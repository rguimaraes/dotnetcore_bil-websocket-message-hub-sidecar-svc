﻿using Message.Hub.Api.HubEndPoints.Base;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;


namespace Message.Hub.Api.HubEndPoints
{
    public class ClientHubEndPoint: BaseHubEndPoint
    {
        ILogger<ClientHubEndPoint> _ilog;

        public ClientHubEndPoint(ILogger<ClientHubEndPoint> ilog)
        {
            _ilog = ilog;
            SignClientHubEndPoint();
        }

        public void SignClientHubEndPoint()
        {
            Hubconnection.On<double>("SyncBag", (message) =>
            {
                _ilog.LogInformation($"Receive Bag " +  message);
            });


            //Hubconnection.On<List<CurrencyClientConnected>>("CurrencyClientConnected", (list) =>
            //{
            //    _ilog.LogInformation($"client" + list);
            //});
        }
    }
}
