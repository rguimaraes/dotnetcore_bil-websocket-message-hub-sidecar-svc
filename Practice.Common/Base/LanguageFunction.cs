﻿namespace Common.Base
{ 
    /// <summary>
    /// Functions for language
    /// </summary>
    public static class LanguageFunctions
    {
        /// <summary>
        /// Enumerator for the language
        /// </summary>
        public enum Language : short
        {
            ptBr = 1,
            esES = 2,
            enUS = 3
        }

        /// <summary>
        /// Get Id for the language
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public static Language GetLanguageID(string language)
        {
            Language idLanguage;
            switch (language)
            {
                case "pt-BR":
                    idLanguage = Language.ptBr;
                    break;
                case "es-ES":
                    idLanguage = Language.esES;
                    break;
                default:
                    idLanguage = Language.enUS;
                    break;
            }
            return idLanguage;
        }

    }
}
