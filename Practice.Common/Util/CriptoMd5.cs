﻿using System.Security.Cryptography;
using System.Text;

namespace Common.Util
{
    public class CriptoMd5
    {
        /// <summary>
        /// Encrypt value with the the algorithm MD5
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Encrypt(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            var builder = new StringBuilder();
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] arrBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(text));
                foreach (byte b in arrBytes)
                {
                    builder.Append(b.ToString("X2"));
                }
            }
            
            return builder.ToString().ToLower();
        }
    }
}
