﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Common.AutoConfigurable.HttpClient.Configuration;
using Common.AutoConfigurable.HttpClient.Contracts;
using Common.AutoConfigurable.HttpClient.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClientRestToSignalR
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration iconfiguration = new ConfigurationBuilder()
                          .SetBasePath(Directory.GetCurrentDirectory())
                          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .Build();


            var serviceProvider = new ServiceCollection()
                .AddSingleton(iconfiguration)
                .AddAutoConfigurableHttpClient()
                .AddSingleton<TestRest>();


            TestRest t = serviceProvider.BuildServiceProvider(true).GetService<TestRest>();

            _ = t.TestGetAsync();
             t.TestPostAsync();

            Console.ReadKey();

        }
    }

    public class TestRest
    {
        IHttpClientContext _restContext;

        public TestRest(IHttpClientContext restContext)
        {
            _restContext = restContext;
        }

        public async Task TestGetAsync()
        {
            HttpResponseMessage response = await _restContext.GetAsync(_restContext.GetUrlService("Hub") + "api/v1/clients/all");
            var json = await response.Content.ReadAsJsonAsync<List<CurrencyClientConnected>>();
        }

        public void TestPostAsync()
        {
            double c = 0;
            while (c < 5000)
            {
                HttpResponseMessage response = _restContext.PostAsync(_restContext.GetUrlService("Hub") + "api/v1/jackpot/syncbag",
                        new { connectionid = "", groupname = "", value = c }).Result;

                c += 1;
            }
        }
    }


    public class CurrencyClientConnected
    {
        public DateTime UTCStartConnected { get; set; }
        public string ConnectionId { get; set; }
        public string GroupName { get; set; }
        public string IPremote { get; set; }
    }


}
