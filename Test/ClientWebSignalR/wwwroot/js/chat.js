﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("http://192.168.8.158/Message.Hub/gameHub").build();

connection.serverTimeoutInMilliseconds = 1000 * 60 * 24 * 360 ;

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("SyncBag", function (value) {
    var encodedMsg = " Currency Bag Jackpot " + value;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});


connection.on("CurrencyClientConnected", function (list) {
    
    while (document.getElementById("clients").hasChildNodes()) {
        document.getElementById('clients').innerHTML = '';
    } 

    list.forEach((v) => {

        var li = document.createElement("li");
        li.textContent = v.connectionId + " - " + v.remoteIpAddress;
        document.getElementById("clients").appendChild(li);

    })
    
});


connection.on("triggerecho", function () {

    
    var li = document.createElement("li");
    li.textContent = "Echo fired";
    document.getElementById("echo").appendChild(li);

});




connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var bag = document.getElementById("userInput").value;
   
    connection.invoke("SendBag", bag).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});