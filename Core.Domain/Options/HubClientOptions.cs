﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Options
{
    public class HubClientOptions
    {
        public const string OPTIONS_PREFIX = "HubClient";
        public string UrlServer { get; set; }
        public double ServerTimeout { get; set; }
        public double RetryConnectDelay { get; set; }
    }
}
